# Performance evaluation on glmnet LASSO vs self defined LASSO
Implemented the Lasso using Coordinate Descent algorithm and the algorithm was applied on the Boston housing data. 
A comparision was made between the prediction done using the Lasso from glmnet package and our Lasso.